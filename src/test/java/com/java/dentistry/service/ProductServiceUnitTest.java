package com.java.dentistry.service;

import com.java.dentistry.entity.Product;
import com.java.dentistry.repository.ProductRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceUnitTest {
    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private ProductService productService;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getAllProductTest(){
        List<Product> list = new ArrayList<>();

        list.add(new Product((long) 1, "dental floss","China",2000.0,"Lorem Ipsum is simply dummy text of the printing and typesetting industry","1.jpg"));
        list.add(new Product((long) 2, "Stainless Steel","Australia",3000.0,"Lorem Ipsum is simply dummy text of the printing and typesetting industry","2.jpg"));

        Mockito.when(productRepository.findAll()).thenReturn(list);

        List<Product> serviceList = productService.getList();

        Assert.assertEquals(2,serviceList.size());

        Mockito.verify(productRepository,Mockito.times(1)).findAll();
    }

}
