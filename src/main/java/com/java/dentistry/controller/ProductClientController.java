package com.java.dentistry.controller;

import com.java.dentistry.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/dentistry")
public class ProductClientController {

    @Autowired
    ProductService productService;

    @GetMapping("/product-home")
    public String view(Model model){
        model.addAttribute("list", productService.getList());
        return "product/product-client";
    }
}
