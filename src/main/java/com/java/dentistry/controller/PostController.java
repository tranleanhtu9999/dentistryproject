package com.java.dentistry.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.java.dentistry.entity.Category;
import com.java.dentistry.entity.Post;
import com.java.dentistry.repository.PostRepository;
import com.java.dentistry.service.PostService;

@Controller
@RequestMapping("/admin")
public class PostController {
	@Autowired
	private PostService postService;
	@Autowired
	private PostRepository postRepository;

	@GetMapping("/add-post")
	public String addPost(Model model) {
		List<Category> list2 = postRepository.listCategory();
		System.out.println(list2);
		model.addAttribute("listCategory", list2);
		return "post/add-post";
	}

	@PostMapping("/add")
	public String addPost(@RequestParam("name") String name, @RequestParam("content") String content,
			@RequestParam("detail_content") String detail_content, @RequestParam("categoryId") int categoryId,
			@RequestParam("imageFile") MultipartFile imageFile) {
		Post post = new Post();
		post.setName(name);
		post.setContent(content);
		post.setDetail_content(detail_content);

		post.setCategory(new Category(categoryId));
		if (imageFile != null && !imageFile.isEmpty()) {
			String or = imageFile.getOriginalFilename();
			int lastIndex = or.lastIndexOf(".");
			String ext = or.substring(lastIndex);
			String image = System.currentTimeMillis() + ext;
			String folderLocation = "C:/Users/giangtranv/Documents/AVATAR/";
			FileOutputStream fileOutputStream;
			try {
				fileOutputStream = new FileOutputStream(folderLocation + image);
				fileOutputStream.write(imageFile.getBytes());
				fileOutputStream.flush();
				fileOutputStream.close();
				// luu lai ten file vao db
				post.setImage(image);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		LocalDateTime date = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
		post.setCreate_date(date.format(formatter));
		postService.addPost(post);
		return "redirect:/admin/list-post";

	}

	@GetMapping("/list-post")
	public String listPost(Model model) {
		List<Post> list = postService.listPost();
		model.addAttribute("listPost", list);
		return "post/list-post";
	}

	@GetMapping("/edit-post/{id}")
	public String updatePost(Model model, @PathVariable(value = "id") int id) {
		Post post = postService.getPostById(id);
		List<Category> list = postRepository.listCategory();
		model.addAttribute("category", list);
		model.addAttribute("post", post);
		return "post/edit-post";
	}

	@PostMapping("/edit")
	public String edit(Model model, @ModelAttribute Post post) {
		if (post.getImageFile() != null && !post.getImageFile().isEmpty()) {
			String or = post.getImageFile().getOriginalFilename();
			int index = or.lastIndexOf(".");
			String ext = or.substring(index);
			String avatar = System.currentTimeMillis() + ext;
			String foderLocal = "C:/Users/giangtranv/Documents/AVATAR/";

			String name;
			try {
				FileOutputStream fileOutputStream = new FileOutputStream(foderLocal + avatar);
				try {
					fileOutputStream.write(post.getImageFile().getBytes());
					fileOutputStream.flush();
					fileOutputStream.close();

					post.setImage(avatar);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		postService.updatePost(post);
		return "redirect:/admin/list-post";

	}

	@GetMapping("/delete-post/{id}")
	public String deletePost(@ModelAttribute Post post, @PathVariable(value = "id") int id) {
		postService.deletePost(post.getId());
		return "redirect:/admin/list-post";

	}

	@GetMapping("/image")
	public void downloadImage(@RequestParam("filename") String filename, HttpServletResponse response) {
		String foderLocal = "C:/Users/giangtranv/Documents/AVATAR/";
		try {
			File file = new File(foderLocal, filename);
			if(!filename.equals("null")){
				Files.copy(file.toPath(), response.getOutputStream());				
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
