package com.java.dentistry.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.java.dentistry.entity.Contact;
import com.java.dentistry.service.ContactService;

@Controller
@RequestMapping("/admin")
public class ContactController {
	@Autowired
	private ContactService contactService;

	@Autowired
	private JavaMailSender sender;

	@GetMapping("/contact")
	public String contact() {
		return "contactTest";
	}

	@PostMapping("/contact")
	public String contact(Model model, @RequestParam("name") String name, @RequestParam("email") String email,
			@RequestParam("message") String message) {
		Contact contact = new Contact();
		contact.setName(name);
		contact.setEmail(email);
		contact.setMessage(message);
		LocalDateTime date = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
		contact.setDate(date.format(formatter));
		contactService.addContact(contact);

		// SendMail
		MimeMessage ms = sender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(ms);
		try {
			helper.setTo(contact.getEmail());
			helper.setText(
					"Cảm ơn bạn đã quan tâm đến phòng khám của chúng tôi. Chúng tôi sẽ sớm thông tin đến bạn sớm nhất có thể. Xin cảm ơn! "

			);
			helper.setSubject("Phòng khám DET10");

		} catch (MessagingException e) {
			e.printStackTrace();

			return "Error while sending mail ..";
		}
		sender.send(ms);
		return "redirect:/contact";

	}

	@GetMapping("/list-contact")
	public String listContactByDate(Model model) {
		List<Contact> list = contactService.listContactByDate();
		model.addAttribute("listContact", list);
		return "contact/list-contact";
	}

}
