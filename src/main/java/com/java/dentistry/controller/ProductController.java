package com.java.dentistry.controller;

import com.java.dentistry.dto.ProductDTO;
import com.java.dentistry.entity.Product;
import com.java.dentistry.repository.ItemsRepository;
import com.java.dentistry.repository.OrdersRepository;
import com.java.dentistry.service.MenuServices;
import com.java.dentistry.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

@Controller
public class ProductController {

    @Autowired
    ProductService productService;

    @Autowired
    OrdersRepository ordersRepository;

    @Autowired
    ItemsRepository itemsRepository;

    private static String UPLOAD_FOLDER = "D:/intelliJ/dentistry-project/src/main/resources/static/img/";

    @GetMapping("/list-product")
    public String listProduct(Model model){
        model.addAttribute("listProduct", productService.getList());
        model.addAttribute("product", new ProductDTO());

        return "list-product";
    }

    @GetMapping("/detail-product/{id}")
    public String detailProduct(@PathVariable(name = "id") Long id, @ModelAttribute("product") Product product, Model model){
        product = productService.getbyId(id);
        model.addAttribute("productDetail", product);

        return "detail-product";
    }

    @GetMapping("/show-edit-form/{id}")
    public String showEditProductForm(@PathVariable(name = "id") Long id, Model model){
        model.addAttribute("product",productService.getbyId(id));

        return "product/edit-product";
    }

    @PostMapping("/edit-product/{id}")
    public String editProduct(@PathVariable(name = "id") Long id, @ModelAttribute("product") ProductDTO productDTO, Model model){
        Product pro = productService.getbyId(id);

        MultipartFile file = productDTO.getFile();
        if(file == null){
            model.addAttribute("NoImport", "You have not selected a photo");
        }
        FileOutputStream outputStream = null;
        try {
            File new_file = new File(UPLOAD_FOLDER + file.getOriginalFilename());
            outputStream = new FileOutputStream(new_file);
            outputStream.write(file.getBytes());

            productDTO.setImgURL(file.getOriginalFilename());
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Product product = new Product(null,
                productDTO.getName(),
                productDTO.getMadeIn(),
                productDTO.getPrice(),
                productDTO.getDetail()
                ,productDTO.getImgURL());
        productService.deleteProduct(id);
        productService.addProduct(product);

        return "redirect:/admin/list-product";
    }

    @GetMapping("/delete/{id}")
    public String deleleProduct(@PathVariable(name = "id") Long id) {
        productService.deleteProduct(id);

        return "redirect:/admin/list-product";
    }
}
