package com.java.dentistry.controller;

import com.java.dentistry.dto.OrdersDTO;
import com.java.dentistry.entity.Items;
import com.java.dentistry.entity.Orders;
import com.java.dentistry.repository.ItemsRepository;
import com.java.dentistry.service.ItemsService;
import com.java.dentistry.service.OrdersService;
import com.java.dentistry.util.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;

@Controller
@RequestMapping("/orders")
public class OrdersController {
    @Autowired
    OrdersService ordersService;

    @Autowired
    ItemsService itemsService;

    @GetMapping("/confirm-orders")
    public String confirmOrders(Model model){
        model.addAttribute("orders",new OrdersDTO());

        return "product/confirm-orders";
    }

    public String get_orders(Model model){
        model.addAttribute("get-orders",ordersService.list_orders());

        return "product/orders";
    }

    @PostMapping("/sub-confirm")
    public String sub_confirm(@ModelAttribute("orders") OrdersDTO ordersDTO, HttpServletRequest req){
        HttpSession session = req.getSession();
        OrdersDTO customer_orders = (OrdersDTO) session.getAttribute("orders");

        Orders orders = new Orders();
        orders.setName(ordersDTO.getName());
        orders.setAddress(ordersDTO.getAddress());
        orders.setPhoneNumber(ordersDTO.getPhoneNumber());

        ordersService.orders(orders);

        Orders orders1 = ordersService.getbyName(orders.getName());


        double total = 0;
        ArrayList<Items> list = new ArrayList<>();
        for (int i = 0; i < customer_orders.getItems().size(); i++) {
            Items items = new Items();
            items.setProduct(customer_orders.getItems().get(i).getProduct());
            items.setPrice(customer_orders.getItems().get(i).getPrice());
            items.setQuantity(customer_orders.getItems().get(i).getQuantity());
            items.setOrders(orders);


            total += items.getQuantity() * items.getPrice();
            list.add(items);
            itemsService.saveItems(items);
        }

        orders1.setItems(list);
        orders1.setTotal(total);

        ordersService.orders(orders1);

        return "redirect:/dentistry/product-home";
    }
}