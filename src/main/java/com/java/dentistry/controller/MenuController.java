package com.java.dentistry.controller;

import com.java.dentistry.entity.Menu;
import com.java.dentistry.service.MenuServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

import org.springframework.ui.Model;

@Controller
public class MenuController {
    @Autowired
    private MenuServices menuServices;

    @GetMapping("/view")
    public String menuHome() {
        return "menu/Menu";
    }

    @GetMapping("/demo")
    public String menuHome2() {
        return "menu/test";
    }

    @GetMapping("/test")
    public String menu() {
        return "table-basic";
    }

    @GetMapping("/memuview")
    @ResponseBody
    public List<Menu> menuView() {
        return menuServices.getAllMenu();
    }

    @GetMapping("/template")
    public String demo(Model model) {
        System.out.println("111111111111111111");
        model.addAttribute("listCustomer", menuServices.getAllMenu());
        System.out.println("2222222222222222222");
        return "menu/a";
    }

    @GetMapping("/tem")
    public String tem() {
//        System.out.println("111111111111111111");
//        model.addAttribute("listCustomer",menuServices.getAllMenu());
//         System.out.println("2222222222222222222");
        return "menu/tem";
    }


}
