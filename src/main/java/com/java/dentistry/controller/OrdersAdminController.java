package com.java.dentistry.controller;

import com.java.dentistry.repository.ItemsRepository;
import com.java.dentistry.repository.OrdersRepository;
import com.java.dentistry.service.ItemsService;
import com.java.dentistry.service.OrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/admin")
public class OrdersAdminController {
    @Autowired
    OrdersService ordersService;

    @Autowired
    ItemsService itemsService;

    @GetMapping("/list-orders")
    public String showOrders(Model model){
        model.addAttribute("listorders", ordersService.list_orders());
        model.addAttribute("listitems",itemsService.get_all());

        return "product/list-orders";
    }
}
