package com.java.dentistry.controller;

import com.java.dentistry.dto.OrdersDTO;
import com.java.dentistry.entity.Items;
import com.java.dentistry.entity.Orders;
import com.java.dentistry.entity.Product;
import com.java.dentistry.repository.ItemsRepository;
import com.java.dentistry.service.OrdersService;
import com.java.dentistry.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/cart")
public class CartController {
    @Autowired
    ProductService productService;

    @Autowired
    OrdersService ordersService;

    @Autowired
    ItemsRepository itemsRepository;

    @GetMapping("/add-To-Cart/{id}")
    public String addToCart(HttpServletRequest request, @PathVariable(name = "id") Long id, @ModelAttribute("product") Product product){
        double quantity = 1;
        Long ma;
        if(productService.getbyId(id) != null) {
            HttpSession session = request.getSession();
            product = productService.getbyId(id);
            if(session.getAttribute("orders") == null){
                OrdersDTO ordersDTO = new OrdersDTO();
                ArrayList<Items> listItems = new ArrayList<>();
                Items items = new Items();
                items.setQuantity(quantity);
                items.setProduct(product);
                items.setPrice(product.getPrice());
                items.setOrders(new Orders(ordersDTO.getName(),ordersDTO.getItems(),ordersDTO.getPhoneNumber(),ordersDTO.getAddress(),ordersDTO.getTotal()));
                listItems.add(items);
                ordersDTO.setItems(listItems);
                session.setAttribute("orders",ordersDTO);
            }else{
                OrdersDTO ordersDTO = (OrdersDTO) session.getAttribute("orders");
                List<Items> list = ordersDTO.getItems();
                boolean check = false;
                for (Items items : list) {
                    if(items.getProduct().getId().equals(product.getId())){
                        items.setQuantity(items.getQuantity() + quantity);
                        check = true;
                    }
                }
                if(check == false){
                    Items items = new Items();
                    items.setQuantity(quantity);
                    items.setProduct(product);
                    items.setPrice(product.getPrice());
                    items.setOrders(new Orders(ordersDTO.getName(),ordersDTO.getItems(),ordersDTO.getPhoneNumber(),ordersDTO.getAddress(),ordersDTO.getTotal()));
                    list.add(items);
                }
                session.setAttribute("orders", ordersDTO);
            }
        }
        return "redirect:/dentistry/product-home#product";
    }

    @GetMapping("/list-cart")
    public String listCart(Model model, HttpServletRequest request){
        HttpSession session = request.getSession(true);
        if(session.getAttribute("orders") == null){
            model.addAttribute("response","NoData");
        }
        return "product/cart";
    }
}