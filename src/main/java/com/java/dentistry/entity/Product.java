package com.java.dentistry.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "product")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String madeIn;
    private double price;
    private String detail;
    private String img_URL;

    @OneToMany(mappedBy = "product")
    List<Items> items;

    public Product() {
    }

    public Product(String name, String madein, double price, String detail, String imgURL) {
        this.name = name;
        this.madein = madein;
        this.price = price;
        this.detail = detail;
        this.imgURL = imgURL;
    }

    public Product(Long id, String name, String madeIn, double price, String detail, String imgURL) {
        this.id = id;
        this.name = name;
        this.madeIn = madeIn;
        this.price = price;
        this.detail = detail;
        this.img_URL = imgURL;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMadeIn() {
        return madeIn;
    }

    public void setMadeIn(String madeIn) {
        this.madeIn = madeIn;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getImgURL() {
        return img_URL;
    }

    public void setImgURL(String imgURL) {
        this.img_URL = imgURL;
    }
}
