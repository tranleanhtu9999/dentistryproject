package com.java.dentistry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DentistryprojectApplication {

    public static void main(String[] args) {
        SpringApplication.run(DentistryprojectApplication.class, args);
    }

}
