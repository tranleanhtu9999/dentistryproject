package com.java.dentistry.dto;

import com.java.dentistry.entity.Items;

import java.util.ArrayList;
import java.util.List;

public class OrdersDTO {
    private Long id;
    private String name;
    private List<Items> items;
    private String phoneNumber;
    private String address;
    private double total;

    public OrdersDTO(Long id, String name, ArrayList<Items> items, String phoneNumber, String address, double total) {
        this.id = id;
        this.name = name;
        this.items = items;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.total = total;
    }

    public OrdersDTO() {
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Items> getItems() {
        return items;
    }

    public void setItems(List<Items> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "OrdersDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", items=" + items +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", address='" + address + '\'' +
                ", total=" + total +
                '}';
    }

    public double total(){
        double t = 0;
        for (int i = 0; i < items.size(); i++) {
            t += (items.get(i).getPrice() * items.get(i).getQuantity());
        }
        return t;
    }
}