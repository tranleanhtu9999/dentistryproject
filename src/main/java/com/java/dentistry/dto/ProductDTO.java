package com.java.dentistry.dto;

import org.springframework.web.multipart.MultipartFile;

public class ProductDTO {
    private Long id;
    private String name;
    private String madeIn;
    private double price;
    private String detail;
    private String imgURL;
    private MultipartFile file;

    public ProductDTO() {
    }

    public ProductDTO(Long id, String name, String madeIn, double price, String detail, String imgURL, MultipartFile file) {
        this.id = id;
        this.name = name;
        this.madeIn = madeIn;
        this.price = price;
        this.detail = detail;
        this.imgURL = imgURL;
        this.file = file;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMadeIn() {
        return madeIn;
    }

    public void setMadeIn(String madeIn) {
        this.madeIn = madeIn;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getImgURL() {
        return imgURL;
    }

    public void setImgURL(String imgURL) {
        this.imgURL = imgURL;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
