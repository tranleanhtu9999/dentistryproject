package com.java.dentistry.message;

/**
 * Created by anhtutranl on 10/28/2019.
 */
public class Response {
	private String message;

	public Response(String message, String status) {
		this.message = message;
		this.status = status;
	}

	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
