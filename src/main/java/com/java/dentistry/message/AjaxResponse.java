package com.java.dentistry.message;

/**
 * Created by anhtutranl on 10/28/2019.
 */
public class AjaxResponse {
    private  Object object;
    private String data;
    private  String status;

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public AjaxResponse(Object object, String status, String data) {
        this.object = object;
        this.data = data;
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


}
