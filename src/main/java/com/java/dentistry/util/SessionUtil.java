package com.java.dentistry.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class SessionUtil {
    public static void CancelSession(HttpServletRequest request){
        HttpSession session = request.getSession();
        session.invalidate();
    }
}
