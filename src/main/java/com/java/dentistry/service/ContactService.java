package com.java.dentistry.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.dentistry.entity.Contact;
import com.java.dentistry.repository.ContactRepository;

@Service
@Transactional
public class ContactService {
	@Autowired
	private ContactRepository contactRepository;

	public void addContact(Contact contact) {
		contactRepository.addContact(contact);
	}

	public void deleteContact(int id) {
		Contact contact = contactRepository.getContactById(id);
		if (contact != null) {
			contactRepository.deleteContact(contact);
		}
	}

	public void updateContact(Contact contact) {
		Contact currentContact = contactRepository.getContactById(contact.getId());
		if (currentContact != null) {
			currentContact.setName(contact.getName());
			currentContact.setEmail(contact.getEmail());
			currentContact.setMessage(contact.getMessage());
			LocalDateTime date = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
			currentContact.setDate(date.format(formatter));

		}
		contactRepository.updateContact(currentContact);
	}

	public List<Contact> search(String name) {
		return contactRepository.search("%" + name + "%");

	}

	public Contact getContactById(int id) {
		return contactRepository.getContactById(id);
	}
	
	public List<Contact> listContactByDate(){
		return contactRepository.listContactByDate();
	}

}
