package com.java.dentistry.service;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.dentistry.entity.Category;
import com.java.dentistry.entity.Post;
import com.java.dentistry.repository.PostRepository;

@Service
@Transactional
public class PostService {

	@Autowired
	private PostRepository postRepository;

	public void addPost(Post post) {
		postRepository.addPost(post);
	}

	public void updatePost(Post post) {
		Post currentPost = postRepository.getPostById(post.getId());
		if (currentPost != null) {
			currentPost.setName(post.getName());
			currentPost.setContent(post.getContent());
			currentPost.setDetail_content(post.getDetail_content());
			LocalDateTime date = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
			currentPost.setCreate_date(date.format(formatter));
			currentPost.setCategory(new Category(post.getCategoryId()));
			if (post.getImage() != null) {
				String fileName = currentPost.getImage();
				String foder = "/C:/Users/giangtranv/Documents/AVATAR/";
				File file = new File(fileName, foder);
				if (file.exists()) {
					file.delete();
				}
				currentPost.setImage(post.getImage());
			}
			postRepository.updatePost(currentPost);

		}
	}

	public void deletePost(int id) {
		Post post = postRepository.getPostById(id);
		if (post != null) {
			postRepository.deletePost(post);
		}
	}

	public Post getPostById(int id) {
		return postRepository.getPostById(id);
	}

	public List<Post> listPost() {
		return postRepository.listPostByCreatedate();
	}

}
