package com.java.dentistry.service;

import com.java.dentistry.entity.Menu;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


public interface MenuServices {
    public List<Menu> getAllMenu();
    public Menu save(Menu menu);
    public Menu getMenuById(long id);
    public boolean deleteMenuById(long id);
    public int count(long id);

}
