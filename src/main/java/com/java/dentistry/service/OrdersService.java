package com.java.dentistry.service;

import com.java.dentistry.entity.Orders;
import com.java.dentistry.repository.OrdersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrdersService {
    @Autowired
    OrdersRepository ordersRepository;

    public List<Orders> list_orders(){
        return ordersRepository.findAll();
    }

    public Orders orders(Orders orders){
        return ordersRepository.save(orders);
    }

    public Orders getbyName(String name){
        return ordersRepository.getByName(name);
    }
}