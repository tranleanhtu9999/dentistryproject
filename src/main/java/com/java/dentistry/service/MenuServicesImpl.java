package com.java.dentistry.service;

import com.java.dentistry.entity.Menu;
import com.java.dentistry.repository.MenuReponsitory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service

public class MenuServicesImpl implements MenuServices {


    @Autowired
    private MenuReponsitory menuReponsitory;

    @Override
    public List<Menu> getAllMenu() {
        return menuReponsitory.findAll();
    }

    @Override
    public Menu save(Menu menu) {
        return menuReponsitory.save(menu);
    }

    @Override
    public Menu getMenuById(long id) {

        return menuReponsitory.findMenuById(id);
    }

    @Override
    public boolean deleteMenuById(long id) {
        if(id>0) {
            menuReponsitory.deleteById(id);
            return true;
        }
        else {  return  false; }

    }

    @Override
    public int count(long id) {
        return menuReponsitory.count(id);
    }

}
