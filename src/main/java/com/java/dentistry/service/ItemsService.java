package com.java.dentistry.service;

import com.java.dentistry.entity.Items;
import com.java.dentistry.repository.ItemsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ItemsService {
    @Autowired
    ItemsRepository itemsRepository;

    public List<Items> get_all(){
        return itemsRepository.findAll();
    }

    public Items saveItems(Items items){
        return itemsRepository.save(items);
    }
}
