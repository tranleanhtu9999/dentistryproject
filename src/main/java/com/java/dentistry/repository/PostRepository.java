package com.java.dentistry.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.hibernate.tool.schema.internal.IndividuallySchemaMigratorImpl;
import org.springframework.stereotype.Repository;

import com.java.dentistry.entity.Category;
import com.java.dentistry.entity.Post;

@Repository
@Transactional
public class PostRepository {

	@PersistenceContext
	private EntityManager entityManager;

	public void addPost(Post post) {
		entityManager.persist(post);
	}

	public void updatePost(Post post) {
		entityManager.merge(post);
	}

	public void deletePost(Post post) {
		entityManager.remove(post);
	}

	public Post getPostById(int id) {
		return entityManager.find(Post.class, id);
	}

	public List<Post> listPostByCreatedate() {
		return entityManager.createQuery("SELECT p FROM Post p ORDER BY p.create_date ASC ").getResultList();
	}
	
	public List<Category> listCategory(){
		return entityManager.createQuery("FROM Category").getResultList();
	}
	
	public Category findCategoryById(int id_category){
		return entityManager.find(Category.class, id_category);
	}

}
