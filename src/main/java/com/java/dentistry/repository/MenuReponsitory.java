package com.java.dentistry.repository;

import com.java.dentistry.entity.Menu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository("menurepository")
public interface MenuReponsitory extends JpaRepository<Menu,Long> {
    @Query(value = "select count(*) from menu u  where u.menuId = :menuId",nativeQuery = true)
        int count(@Param("menuId") long menuId);
    @Query(value = "select * from menu u where u.menu_id = :id", nativeQuery = true)
    Menu findMenuById(@Param("id") Long id);

//    @Query(value = "insert into menu  where u.menuId = :menuId",nativeQuery = true)
//    int count(@Param("menuId") long menuId);




}
