package com.java.dentistry.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.java.dentistry.entity.Contact;

@Repository
@Transactional
public class ContactRepository {
	@PersistenceContext
	private EntityManager entityManager;

	public void addContact(Contact contact) {
		entityManager.persist(contact);
	}

	public void updateContact(Contact contact) {
		entityManager.merge(contact);
	}

	public void deleteContact(Contact contact) {
		entityManager.remove(contact);
	}

	public Contact getContactById(int id) {
		return entityManager.find(Contact.class, id);
	}

	public List<Contact> search(String name) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Contact> query = builder.createQuery(Contact.class);
		Root<Contact> root = query.from(Contact.class);
		if (name != null) {
			Predicate predicate = builder.like(builder.lower(root.get("name")), "%" + name.toLowerCase() + "%");
			query.where(predicate);
		}
		query.select(root);
		TypedQuery<Contact> typedQuery = entityManager.createQuery(query);
		return typedQuery.getResultList();
	}
	
	public List<Contact> listContactByDate(){
		return entityManager.createQuery("SELECT c FROM Contact c ORDER BY c.date ASC ").getResultList();
	}
	
	

}
