/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



$(document).ready(function () {


$('#dtBasicExample').DataTable();
$('.dataTables_length').addClass('bs-select');



$('#createNewProduct').click(function () {

        $('#saveBtn').val("create-product");

        $('#product_id').val('');

        $('#productForm').trigger("reset");

        $('#modelHeading').html("Create New Product");

        $('#ajaxModel').modal('show');

    });

  $('.edit').on("click", function(e){
            e.preventDefault();
            var Id = parseInt($(this).closest("td").attr("id"));

            console.log(Id);

                    	$.ajax({
                			type : "POST",
                			contentType : "application/json",
                			url :"/edit/"+Id,
                			dataType : 'json',
                			success :
                			function(data) {
                			console.log(data);
                			var obj = $.parseJSON(JSON.stringify(data));
                            console.log(obj.object.menuName);
                				if(data.status == "Done"){
                				   $('#ajaxModel').modal('show');
                				   $('#product_id').val(obj.object.menuName);



                				}else{
                					$("#postResultDiv").html("<strong>Error</strong>");
                				}
                				console.log(data);
                			},
                			error : function(e) {
                				alert("Error!")
                				console.log("ERROR: ", e);
                			}

                		});

                       console.log(window.location);


    });

        $('.delete-order').on("click", function(e){
                    e.preventDefault();
                  var Id = parseInt($(this).closest("td").attr("id"));
                    console.log(Id);

                     if(confirm("Are you sure the order is complete?")){


                            	$.ajax({
                        			type : "POST",
                        			contentType : "application/json",
                        			url :"/delete/"+Id,
                        			data : Id.toString(),
                        			dataType : 'json',
                        			success :
                        			function(data) {

                        				$(".delete-order").closest("td#"+data.data).parent("tr").fadeOut("slow",function(){
                                             $(".delete-order").closest("td#"+data.data).parent("tr").remove();
                        					 $("#postResultDiv").html("<p style='background-color:#7FA7B0; color:white; padding:20px 20px 20px 20px'>" +
                        												"Delete Complete <br>" + "</p>");
                        												})

                        					$("#postResultDiv").html("<strong>Error</strong>");

                        				console.log(data);
                        			},
                        			error : function(e) {
                        				alert("Error!")
                        				console.log("ERROR: ", e);
                        			}

                        		});

                               console.log(window.location);
                               }

            });







     $('#dtBasicExample tbody').on( 'click', 'tr', function () {
            $(this).toggleClass('selected');
        } );


     $("#productForm").submit(function(event) {
    		// Prevent the form from submitting via the browser.
    		event.preventDefault();

    		ajaxPost();
    	});


        function ajaxPost(){

        	// PREPARE FORM DATA
        	var formData = {
        		menuName : $("#menuName").val(),

        	}
        	console.log(formData);




        	$.ajax({
    			type : "POST",
    			contentType : "application/json",
    			url :"/save",
    			data : JSON.stringify(formData),
    			dataType : 'json',
    			success : function(result) {
    				if(result.status == "Done"){
    					$("#postResultDiv").html("<p style='background-color:#7FA7B0; color:white; padding:20px 20px 20px 20px'>" +
    												"Post demo Successfully! <br>" +
    												"---> Customer's Info: FirstName22222 = " +
    											 "</p>");
    				}else{
    					$("#postResultDiv").html("<strong>Error</strong>");
    				}
    				console.log(result);
    			},
    			error : function(e) {
    				alert("Error!")
    				console.log("ERROR: ", e);
    			}
    		});

           console.log(window.location);
        	resetData();

        }

        function resetData(){
        	$("#menuName").val("");

        }


    });









